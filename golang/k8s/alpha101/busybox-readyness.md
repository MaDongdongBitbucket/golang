## readinessProbe checking demo

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    run: centos
  name: centos
spec:
  replicas: 1
  selector:
    matchLabels:
      run: centos
  template:
    metadata:
      labels:
        run: centos
    spec:
      containers:
      - command:
        - tail
        - -f
        - /dev/null
        image: centos
        name: centos
        readinessProbe:
          exec:
            command:
            - cat
            - /tmp/healthy
          initialDelaySeconds: 5
          periodSeconds: 5


```



```shell
kubectl create -f busybox-readiness.yaml

[root@k8s-master alpha101]# kubectl get deployment 
NAME               READY   UP-TO-DATE   AVAILABLE   AGE
busybox            0/1     1            0           15s
nginx-deployment   2/2     2            2           97d


[root@k8s-master alpha101]# kubectl get pod
NAME                                READY   STATUS    RESTARTS   AGE
busybox-75555889cd-cr7bl            0/1     Running   0          48s
nginx-deployment-66b6c48dd5-2c2mf   1/1     Running   0          9d
nginx-deployment-66b6c48dd5-pvllm   1/1     Running   0          9d

[root@k8s-master alpha101]# kubectl exec -it busybox-75555889cd-cr7bl -- cat /tmp/healthy
cat: can't open '/tmp/healthy': No such file or directory
command terminated with exit code 1
[root@k8s-master alpha101]# kubectl exec -it busybox-75555889cd-cr7bl -- touch /tmp/healthy
[root@k8s-master alpha101]# kubectl get pod
NAME                                READY   STATUS    RESTARTS   AGE
busybox-75555889cd-cr7bl            1/1     Running   0          4m44s
nginx-deployment-66b6c48dd5-2c2mf   1/1     Running   0          9d
```
When create the touch /tmp/healthy, the healty checking will return status 0, 
then the ready status of pod will change to 1/1

